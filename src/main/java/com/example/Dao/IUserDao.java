package com.example.Dao;
import javax.jws.soap.SOAPBinding.Use;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.Entity.User;

@Repository
public interface IUserDao extends JpaRepository<User, Integer> 
{
	User findByUsername(String username);
	
	User findByEmail(String email);
	

}
