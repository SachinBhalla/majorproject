package com.example.Dao;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.Entity.CartItem;
import com.example.Entity.Databasefile;
@Repository
public interface IAddToCartDao extends JpaRepository<CartItem, String>
{
	@Query("SELECT dd FROM CartItem dd WHERE dd.username = ?1")
	public List<CartItem>getcartbyusername(String filename);
	
	public CartItem findByfilename(String filename);
	
	public CartItem findByusername(String username);
	
	@Query("SELECT dd FROM CartItem dd WHERE dd.filename = ?1 and dd.username = ?2")
	public CartItem getdressforchecking(String filename,String username);

}
