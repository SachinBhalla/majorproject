package com.example.Dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.Entity.Rating;
@Repository
public interface IRating extends JpaRepository<Rating, Integer>
{

}
