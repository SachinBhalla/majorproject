package com.example.Dao;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.example.Entity.Databasefile;

@Repository
public interface IDatabasefilerepo extends JpaRepository<Databasefile, String>
{
	@Query("SELECT dd FROM Databasefile dd WHERE dd.gender = 'Men'")
	public List<Databasefile> getgenderdress();
	
	@Query("SELECT dd FROM Databasefile dd WHERE dd.gender = 'Women'")
	public List<Databasefile> getWomendress();
	
	public Databasefile findByIdContainsOrFileNameContainsAllIgnoreCase(String name, String lName);
	
	@Query("SELECT dd FROM Databasefile dd WHERE dd.fileName = ?1")
	public List<Databasefile> getsamedress(String filename);
	
	@Query("SELECT dd FROM Databasefile dd WHERE dd.fileName = ?1")
	public Databasefile getrealtedDress(String filename);

}
