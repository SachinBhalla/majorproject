package com.example.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.Dao.ConfirmationTokenRepository;
import com.example.Entity.ConfirmationToken;
import com.example.Entity.EmailSenderService;
import com.example.Entity.User;
import com.example.Entity.forgotentity;
import com.example.Service.IUserService;
import com.example.Service.UserServiceImplementation;
import com.example.Validation.error.ErrorEntity;
@Controller
@RequestMapping("/forgot")
public class ForgotPasswordController 
{
	@Autowired
	IUserService userservice;
	
	@Autowired
	ConfirmationTokenRepository confirmationtokenrepo;
	
	@Autowired
	EmailSenderService emailsenderserive;
	
	@Autowired
	BCryptPasswordEncoder bcrypt;
	
	@GetMapping(value="/showformforforgot")
	public String showformforforgot(Model themodel) {
		forgotentity forgotentity=new forgotentity();
		themodel.addAttribute("theuser", forgotentity);
		return "forgotpassword";
		
	}
	
	@PostMapping("/forgotpassword")
	public String forgotpassword( @ModelAttribute("theuser") User user, Model themodel)
	{
		if(user.getEmail()==null || user.getEmail().trim().isEmpty())
		{
			ErrorEntity errorEntity = new ErrorEntity("Please Enter An Email");
			themodel.addAttribute("errorEntity", errorEntity.getMessage());
			return "forgotpassword";
		}
       User existingUser = userservice.findByEmail(user.getEmail());
	   if(existingUser != null) {
			ConfirmationToken confirmationToken = new ConfirmationToken(existingUser);
			confirmationtokenrepo.save(confirmationToken);
			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setTo(existingUser.getEmail());
			mailMessage.setSubject("Complete Password Reset!");
			mailMessage.setFrom("sachinbhalla896@gmail.com");
			mailMessage.setText("To complete the password reset process, please click here: "
						+"http://localhost:9091/forgot/confirm-reset?token="+confirmationToken.getConfirmationToken());
						
			emailsenderserive.sendEmail(mailMessage);
			themodel.addAttribute("success", "Please Check Your Mail");
				return "forgotpassword";
		}
		else
		themodel.addAttribute("failure", "This Email Does Not Exist In Our Database");
	     return "forgotpassword";
		
  }
	
	@RequestMapping(value="/confirm-reset", method= {RequestMethod.GET, RequestMethod.POST})
	public String validateResetToken(@RequestParam("token")String confirmationToken,Model theModel )
	{
		ConfirmationToken token = confirmationtokenrepo.findByConfirmationToken(confirmationToken);
		
		if(token != null) {
			User user=userservice.findByEmail(token.getUser().getEmail());
			
			userservice.SaveUser(user);
			
			theModel.addAttribute("theusers",user);
			
			return "resetpasswordsucess";
		} 
		else 
		{
			theModel.addAttribute("failuretoken", "The Link Is Broken Please Try Again");
			return  "suctest";
		}
	}	
	
	 @RequestMapping(value ="/resetpassword", method = RequestMethod.POST)
	    public String resetUserPassword(Model theModel, User user) {
		 
		 if(user.getEmail()==null || user.getEmail().trim().isEmpty()||
			user.getPassword()==null || user.getPassword().trim().isEmpty())
		 {
			 ErrorEntity errorEntity = new ErrorEntity("Category, Gender, Price are required...");
				theModel.addAttribute("errorEntity", errorEntity.getMessage());
				return "resetpasswordsucess";
		 }
	        if (user.getEmail() != null) 
	        {
	            User tokenUser=userservice.findByEmail(user.getEmail());
	        	String encodedPassword = bcrypt.encode(user.getPassword());
	            tokenUser.setPassword(encodedPassword);
	            userservice.SaveUser(tokenUser);
	           theModel.addAttribute("success","Your Password is Reset Now You Can login Within New Credentials");
	          return "suctest";
	        } 
	        else 
	        {
	        	 theModel.addAttribute("failure","Failed To Reset Password Please Try After Some Times");
	            return "suctest";
	        }
	        
	    }

}
