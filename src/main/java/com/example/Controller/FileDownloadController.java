package com.example.Controller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.Dao.IAddToCartDao;
import com.example.Dao.IDatabasefilerepo;
import com.example.Entity.CartItem;
import com.example.Entity.Databasefile;
import com.example.Service.Databaseserviceimplementation;


@Controller
@RequestMapping("/download")
public class FileDownloadController 
{

    @Autowired
    private Databaseserviceimplementation fileStorageService;
    
    @Autowired
    IAddToCartDao cartdao;
    
    
    @Autowired
    IDatabasefilerepo repo;
    
    @Autowired
    private ServletContext servletContext;
    

    @GetMapping("/downloadFile/{fileId}")
    public ResponseEntity<?> downloadFile(@PathVariable String fileId,
    		HttpServletRequest request) throws IOException {
      
    	Databasefile databaseFile = fileStorageService.getFile(fileId);
        byte[] imageByte = null;
        HttpHeaders headers = null;
        File file = new File("C:/mydownloads/"+databaseFile.getId()+".jpg");
        MediaType mediaType = getMediaTypeForFileName(this.servletContext, file.getName());
        if (!file.exists())
            return null;

		imageByte = Files.readAllBytes(file.toPath());
        headers = new HttpHeaders();
        headers.setContentType(mediaType);
        return new ResponseEntity<byte[]>(imageByte, headers, HttpStatus.OK);
    	
    }
    public MediaType getMediaTypeForFileName(ServletContext servletContext, String fileName) {
       String mineType = servletContext.getMimeType(fileName);
        try 
        {
            MediaType mediaType = MediaType.parseMediaType(mineType);
            return mediaType;
        } 
        catch (Exception e) 
        {
            return MediaType.APPLICATION_OCTET_STREAM;
        }
    }
    
    
    
    
    @GetMapping("/downloadFileSecond/{fileId}")
    public ResponseEntity<?> downloadFileSecond(@PathVariable String fileId,
    		HttpServletRequest request) throws IOException {
      
    	CartItem  cartitem = cartdao.findById(fileId).orElse(new CartItem());
        byte[] imageByte = null;
        HttpHeaders headers = null;
        File file = new File("C:/mydownloads2/"+cartitem.getId()+".jpg");
        MediaType mediaType = getMediaTypeForFileName(this.servletContext, file.getName());
        if (!file.exists())
            return null;

		imageByte = Files.readAllBytes(file.toPath());
        headers = new HttpHeaders();
        headers.setContentType(mediaType);
        return new ResponseEntity<byte[]>(imageByte, headers, HttpStatus.OK);
    	
    }
   

    
}