package com.example.Controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.Dao.IDatabasefilerepo;
import com.example.Dao.IUserDao;
import com.example.Entity.Databasefile;
import com.example.Entity.User;
import com.example.Service.Databaseserviceimplementation;

@Controller
@RequestMapping("/HomeController")
public class LoginController {
	@Autowired
	IDatabasefilerepo repo;
	@Autowired
	private IUserDao userRepository;
	@Autowired
	private BCryptPasswordEncoder bcryptPassEncoder;
    @Autowired
	Databaseserviceimplementation databaseserviceimple;

	
	@GetMapping(value="/Testing")
	public String TestKart()
	{
		return "Kart";
	}
	
	@GetMapping(value = "/loginForward")
	public String loginForwardPage(Model model) {
		return "login_forward";
	}
	
	@GetMapping(value = "/payment")
	public String paymentGatewayPage() {
		return "PaymentGateway";
	}
	
	@GetMapping(value = "/error")
	public String errorPage() {
		return "Error";
	}

	@GetMapping(value = "/home")
	public String HomePage() {
		return "MainHome";
	}

	@GetMapping(value = "/loginsucessfullapi")
	public String AdminHome() {
		return "HomeModern";
	}

	@GetMapping("/showMyLoginPage")
	public String showMyLoginPage(Model model, Authentication authentication) {
		if(authentication!=null && authentication.getName()!=null & !authentication.getName().isEmpty())
        return "redirect:/HomeController/list";
		
		return "login";

	}

	@GetMapping("/list")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public String listDress(Model theModel, @RequestParam(name = "page") Optional<Integer> reqPage,
			@RequestParam(name = "size") Optional<Integer> reqSize,Authentication authentication) {
		Integer page = reqPage.orElse(1);
		Integer size = reqSize.orElse(8);
		page = page - 1;

		Pageable pageable = PageRequest.of(page, size, Direction.ASC, "id");
        Page<Databasefile> employeesPage = repo.findAll(pageable);
		theModel.addAttribute("employeesPage", employeesPage);

		int totalPages = employeesPage.getTotalPages();
		if (totalPages > 0) {
			List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
			theModel.addAttribute("pageNumbers", pageNumbers);
		}
		
		String username=authentication.getName();
		StringBuffer forwardusernamedummy=new StringBuffer();
		for(int i=0;i<username.length();i++)
		{
			if(Character.isDigit(username.charAt(i)))
			{
				break;
			}
			else
			{
				forwardusernamedummy.append(username.charAt(i));
			}
		}
		String forwardusername=forwardusernamedummy.toString();
		
		theModel.addAttribute("forwardusername", forwardusername);
		
		System.out.println("Welcome"+forwardusername);
		
		

		return "HomeModern";
	}

	@GetMapping(value = "/test")
	public String test() {
		return "HomeModern";
	}

}
