package com.example.Controller;
import java.util.logging.Logger;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.Entity.User;
import com.example.Service.IUserService;



@Controller
@RequestMapping("/user")
public class UserController 
{
	@Autowired
	IUserService userservice;
	
	@Autowired
	BCryptPasswordEncoder bcrypt;
	
	private Logger logger = Logger.getLogger(getClass().getName());
	    
		@InitBinder
		public void initBinder(WebDataBinder dataBinder) 
		{
			
			StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
			
			dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
		}
	
		
		@GetMapping(value="/home")
		public String HomePage()
		{
			return "home";
		}
	
	@PostMapping(value="/save")
	public User SaveUser(@Valid @RequestBody User user) {
		return userservice.SaveUser(user);
		
	}
	    
	@PostMapping("/saveuserprofile")
	public String saveEmployee(@ModelAttribute("user") User theuser) {
			
			// save the employee
	    	userservice.SaveUser(theuser);
			
			// use a redirect to prevent duplicate submissions
	    	return "redirect:/HomeController/list";
		}
	@GetMapping("/showFormForAdd")
	public String showFormForAdd(Model theModel) {
		
		// create model attribute to bind form data
		User theuser = new User();
		theModel.addAttribute("theuser", theuser);
		return "signup.html";
	}
	@PostMapping("/processRegistrationForm")
	public String processRegistrationForm(
				@Valid @ModelAttribute("theuser") User theuser, 
				BindingResult theBindingResult, 
				Model theModel) {
		
		String userName = theuser.getUsername();
		logger.info("Processing registration form for: " + userName);
		
		
		// form validation
		 if (theBindingResult.hasErrors()){
			 return "signup";
	        }

		// check the database if user already exists
		 User existing = userservice.findByUserName(userName);
        if (existing != null){
        	theModel.addAttribute("theuser", new User());
			theModel.addAttribute("registrationError", "User name already exists.");

			logger.warning("User name already exists.");
        	return "signup";
        }
        
        // create user account        						
        if(theuser!= null) 
        {
        	theuser.setRoles("ROLE_USER");
        	String encodedPassword = bcrypt.encode(theuser.getPassword());
        	theuser.setPassword(encodedPassword);
        	userservice.SaveUser(theuser);
			logger.info("Successfully created user: " + userName);
			
		}
        
      return "registration-confirmation";		
	}
	
	
	@GetMapping(value="/Edituserprofile")
	@PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
	public String EditUserProfile(Model theModel,Authentication authentication)
	{
		String username = authentication.getName();
		
		User theuser=userservice.findByUserName(username);
		theModel.addAttribute("user", theuser);
		return "updateuserform";
	}

}
