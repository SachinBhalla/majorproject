package com.example.Controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.example.Entity.Databasefile;
import com.example.Service.Databaseserviceimplementation;
import com.example.Validation.error.ErrorEntity;

@Controller
@RequestMapping("/Admin")
public class AdminControllerMain 
{
	@Autowired
	Databaseserviceimplementation databaseserviceimple;
	
	@GetMapping(value="/homeadmin")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
//	@PreAuthorize("hasRole('ROLE_VIEWER') or hasRole('ROLE_EDITOR')")
	public String homeadmin() {
		
		return "AdminHomeModern";
	}
	@GetMapping(value="/AddDress")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String AddDress(Model theModel)
	{
//		Databasefile databasefile=new Databasefile();
//		theModel.addAttribute("databasefile", databasefile);
		return "DressModern";
	}
	@GetMapping("/list")
	public String listDress(Model theModel) 
	{
		
		// get employees from db
		List<Databasefile> theEmployees = databaseserviceimple.findall();
		// add to the spring model
		theModel.addAttribute("employees", theEmployees);
		return "viewall";
	}
	@GetMapping("/listgender")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String listGender(Model theModel)
	{
		List<Databasefile> theEmployees=databaseserviceimple.getgenderdress();
		
		theModel.addAttribute("employees", theEmployees);
		
		return "viewall";
	}
	@GetMapping("/listwomen")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String listWomen(Model theModel)
	{
		List<Databasefile> theEmployees=databaseserviceimple.getWomendress();
		theModel.addAttribute("employees", theEmployees);
		return "viewall";
	}
	@GetMapping(value="/getdressbyid")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String GetDressById()
	{
		return "searchAdressByid";
	}
	@PostMapping(value = "/getoneforsaerch")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String getone(@RequestParam("fileId") String fileId,Model themodel)
	{
		if(fileId==null || fileId.trim().isEmpty())
		{
			ErrorEntity errorEntity = new ErrorEntity("Field Id Is Required...");
			themodel.addAttribute("errorEntity", errorEntity.getMessage());
			return "searchAdressByid";
		}
		Databasefile thedatabasefile= databaseserviceimple.searchBy(fileId);
		themodel.addAttribute("employees",thedatabasefile);
		return "viewall";
	}
}


	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
	 
















