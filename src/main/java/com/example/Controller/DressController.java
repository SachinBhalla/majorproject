package com.example.Controller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.Dao.IAddToCartDao;
import com.example.Entity.CartItem;
import com.example.Entity.Databasefile;
import com.example.Entity.Rating;
import com.example.Service.Databaseserviceimplementation;
import com.example.Service.ICartService;

@Controller
@RequestMapping("/Dress")
public class DressController 
{
	@Autowired
	Databaseserviceimplementation databaseservice;
	
	@Autowired
	IAddToCartDao addtocartdao;
	
	@Autowired
	ICartService cartservice;
	
	
	
//	@Autowired
//	RatingServiceImplementation ratingservice;// This Service Is Used When user rating is Implemented
	
	 private Logger logger = Logger.getLogger(getClass().getName());
	    
		@InitBinder
		public void initBinder(WebDataBinder dataBinder) 
		{
			
			StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
			
			dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
		}
	
	@PostMapping(value="/viewdress")
	public String ViewDress()
	{
		return "ViewDressDetail";
	}
	
	@PostMapping(value="/ViewDressDetailApi")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public String showFormForUpdate(@RequestParam(required=true, value="FileName") String thefilename,
									Model theModel) 
	{
		List<Databasefile> relateddress;
		System.out.println("In View Dress Detail Api");

		relateddress=databaseservice.getsamedress(thefilename);
		
		System.out.println(relateddress);
		
		theModel.addAttribute("samedress", relateddress);
		return "ViewDressDetail";		
//		return "Dummy";
	}
	@GetMapping(value="/AddToCart")
	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
	public String AddtoCart(@RequestParam(required=true, value="FileName") String thefilename,
			Model theModel,Authentication authentication) throws IOException 
    {
		CartItem cartitem=new CartItem();
	    Databasefile relateddress=databaseservice.getrealtedDress(thefilename);
	    
	   CartItem cartitemfind= cartservice.getdressforchecking(relateddress.getFileName(), authentication.getName());
	   
//	   System.out.println(cartitemfind.getFilename()+" "+cartitem.getProductname()+" "+cartitemfind.getPrice()+" "+cartitemfind.getUsername());
//  
//    
	    if(cartitemfind!=null)
	    {
	    	 theModel.addAttribute("samedress", relateddress);
	    	System.out.println("In IF Condition");
	    	theModel.addAttribute("dressexist", "This Dress Is Alreday Added "
	    			+ "To Your Cart Please Check Your Cart");
	    	return "viewDressDetail";
	    }
	    theModel.addAttribute("samedress", relateddress);
	    cartitem.setProductname(relateddress.getDescription());
	    
	    cartitem.setUsername(authentication.getName());
	    
	    cartitem.setPrice(relateddress.getPrice());
	    
	    cartitem.setFilename(relateddress.getFileName());
	   
	    cartitem.setData(relateddress.getData());
	    
	    CartItem citem=cartservice.save(cartitem);
	    
	    File convertFile = new File("c://mydownloads2//"+cartitem.getId()+".jpg");
		convertFile.createNewFile();
		FileOutputStream fout = new FileOutputStream(convertFile);
		
		fout.write(relateddress.getData());
		fout.close();
		
		theModel.addAttribute("addtocartsuccess", "The Dress Is Added To Your Card Successfully");
	    
	    return "ViewDressDetail";
	    
 }
	
@GetMapping(value="/MyCart")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public String MyCart(Model theModel,Authentication authentication)
{
	CartItem cartitem=new CartItem();
	
	int totalprice=0;
	
	List<CartItem> mycart=cartservice.getcartbyusername(authentication.getName());
	
	if(mycart.isEmpty())
	{
		System.out.println("In If Condition");
		theModel.addAttribute("Emptycart", "This cart is Already Empty");
		return "viewallmycart";
	}
	
	theModel.addAttribute("mycart", mycart);
	
	Set<String> itemIds = new HashSet<String>();
	
	if(CollectionUtils.isNotEmpty(mycart))
		itemIds.addAll(mycart.stream().map(c->c.getId()).collect(Collectors.toSet()));


	theModel.addAttribute("totalprice", totalprice);
	
	return "viewallmycart";
}
@PostMapping("/deleteFromMyCart")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public String deleteFromMyCart(@RequestParam("employeeId") String theId) {
	
	// delete the employee
	cartservice.deleteById(theId);
	
	// redirect to /employees/list
	return "redirect:/Dress/MyCart";
	
}

@GetMapping(value="/TeamInfo")
public String TeamInfo()
{
	return "Team";
}

@GetMapping("/showFormForRating")
public String showFormForAdd(Model theModel) 
{
		System.out.println("In Rating Function");
		
		// create model attribute to bind form data
		Rating theRating = new Rating();
		theModel.addAttribute("therating", theRating);
		return "Rating.html";
	}
	
// This Api Is Used  When User Rating Is Saved

	
//	@PostMapping(value="/saverating")
//	@PreAuthorize("hasRole('ROLE_USER')")
//	public String SaveRatingDetails(@Valid @ModelAttribute("therating") Rating therating, 
//									BindingResult theBindingResult, Model theModel)
//	{
//		if(theBindingResult.hasErrors())
//		{
//			System.out.println("In If Condition");
//			return "Rating";
//		}
//		else
//		{
//			ratingservice.SaveUserCommment(therating);
//			return "HomeModern";
//			
//		}
//		
//		
//	}


}
