package com.example.Controller;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.social.facebook.api.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;
import com.example.Dao.IUserDao;
import com.example.Service.googleService;

@Controller
public class GoogleController 
{
	@Autowired
	googleService googleservice;
	
	@Autowired
	private IUserDao userRepository;
	@Autowired
	private BCryptPasswordEncoder bcrypt;
	
	@GetMapping(value="/home")
	public String Home()
	{
		return "login";
	}
	
	@GetMapping(value="/googlelogin")
	public RedirectView googlelogin()
	{
		RedirectView redirectview=new RedirectView();
		String url=googleservice.goolelogin();
		redirectview.setUrl(url);
		System.out.println(url);
		return redirectview;
	}
	@GetMapping(value="/facebook")
	public String facebook(@RequestParam("code") String code)
	{
		System.out.println("in / FaceBook");
		String acesstoken=googleservice.getfacebookAcessToken(code);
		return "redirect:/facebookprofiledata/"+acesstoken;
	}
	@GetMapping(value="/facebookprofiledata/{acesstoken:.+}")
	public /*ModelAndView*/ String facebookProfileData(@PathVariable String acesstoken,Model model, HttpServletRequest request)
	{
		String realpassword="";
		System.out.println("In/FaceBookProfileData");
		User user=googleservice.getfacebookuserprofile(acesstoken);
		
		System.out.println("The Acess Token is"+acesstoken);
		
		//SocialUser userinfo=new SocialUser(user.getFirstName(),user.getLastName(),user.getEmail());
		
		com.example.Entity.User user1 = this.userRepository.findByEmail(user.getEmail());
//		SocialUser user1=socialuserservice.findByEmail(user.getEmail());
		if(user1 == null) 
		{
			String password=user.getEmail();
			StringBuffer dummypassword=new StringBuffer();
			for(int i=0;i<password.length();i++)
			{
				if(Character.isDigit(password.charAt(i)))
				{
					dummypassword.append(password.charAt(i));	
				}
				else
				{
					continue;
				}
			}
			realpassword=dummypassword.toString();
			
			System.out.println("The Password is"+realpassword);
			user1 = new com.example.Entity.User(null, user.getEmail(),bcrypt.encode(realpassword), user.getFirstName(),
					"9"+String.valueOf(this.generateRandomDigits(9)), user.getEmail(), "ROLE_USER");
			this.userRepository.save(user1);

			
			
			System.out.println(user.getFirstName());
			System.out.println(user.getLastName());
			System.out.println(user.getEmail());
			
			model.addAttribute("username", user1.getUsername());
			model.addAttribute("password",  realpassword);
			return "login_forward";
			
			
		}
		model.addAttribute("error", "This Email Already Exist In Our DataBase");
		return "suctest";
	
}
	public static int generateRandomDigits(int n) {
	    int m = (int) Math.pow(10, n - 1);
	    return m + new Random().nextInt(9 * m);
	}

}
