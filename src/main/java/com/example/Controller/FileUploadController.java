package com.example.Controller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import com.example.Entity.Databasefile;
import com.example.Entity.User;
import com.example.Service.Databaseserviceimplementation;
import com.example.Validation.error.ErrorEntity;
@Controller
@RequestMapping("/upload")
public class FileUploadController 
{
	@Autowired
	Databaseserviceimplementation databaseservice;
	
	
	
	@PostMapping(value="/uploadFile")
	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public String uploadFile(@Valid Model theModel,@RequestParam(required=true,value="category") String category,
    		                                       @RequestParam(required=true,value="gender") String gender,
    		                                       @RequestParam(required=true,value="price")  Integer price,
    		                                       @RequestParam(required = true,value="description")String description,
			                                       @RequestPart(required = true, value = "file") MultipartFile file)  throws IOException
	{
		if(category == null || category.trim().isEmpty() || gender == null || price == null
				|| gender.trim().isEmpty() || description==null
				|| description.trim().isEmpty()) 
		{
			ErrorEntity errorEntity = new ErrorEntity("Category, Gender, Price,Description are required...");
			theModel.addAttribute("errorEntity", errorEntity.getMessage());
			return "DressModern";
		}
        Databasefile databasefile=new Databasefile();
		String fileName = "";
		 if(file != null) {
			 fileName = StringUtils.cleanPath(file.getOriginalFilename());
			 databasefile.setFileType(file.getContentType());
			 databasefile.setData(file.getBytes());
		 }
		 System.out.println("Line 52");
		
           databasefile.setFileName(fileName);
		   
		   databasefile.setCategory(category);
		   databasefile.setPrice(price);
		   databasefile.setGender(gender);
		   databasefile.setDescription(description);
		   
	       Databasefile dbdatabasefile=databaseservice.save(databasefile);
		
		
		File convertFile = new File("c://mydownloads//"+databasefile.getId()+".jpg");
		convertFile.createNewFile();
		FileOutputStream fout = new FileOutputStream(convertFile);
		if(file != null)
		fout.write(file.getBytes());
		fout.close();
		
		System.out.println("Befor Ending");
		
		theModel.addAttribute("filesucess", "File is Uploaded Sucessfully");
	
		return "DressModern";
	}
	@GetMapping(value="/login")
    public String showloginpage() {
    	return "login";
    	
    }
    @PostMapping("/showFormForUpdate")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
	public String showFormForUpdate(@RequestParam("employeeId") String theId,
									Model theModel) {
		
		// get the employee from the service
		Databasefile databasefile = databaseservice.getFile(theId);
		
		// set employee as a model attribute to pre-populate the form
		theModel.addAttribute("employee", databasefile);
		
		// send over to our form
		return "updateform";			
	}
    @PostMapping("/save")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
	public String saveEmployee(@ModelAttribute("employee") Databasefile theEmployee) {
		
		// save the employee
    	databaseservice.save(theEmployee);
		
		// use a redirect to prevent duplicate submissions
    	return "redirect:/Admin/list";
	}
    @PostMapping("/delete")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
	public String delete(@RequestParam("employeeId") String theId) {
		
		// delete the employee
    	databaseservice.deleteById(theId);
		
		// redirect to /employees/list
		return "redirect:/Admin/list";
		
	}
   


}
