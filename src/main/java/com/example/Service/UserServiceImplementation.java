package com.example.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.Dao.IUserDao;
import com.example.Entity.User;

@Service
public class UserServiceImplementation implements IUserService
{
	@Autowired
	IUserDao userdao;

	@Override
	public User SaveUser(User user) {
		return userdao.save(user);
	}

	@Override
	public User findByUserName(String userName) {
		User user=userdao.findByUsername(userName);
		return user;
	}

	@Override
	public User findByEmail(String email) {
		User user=userdao.findByEmail(email);
		return user;
	}
	
	

}
