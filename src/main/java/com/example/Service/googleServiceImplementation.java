package com.example.Service;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.social.facebook.connect.FacebookConnectionFactory;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Service;

@Service
public class googleServiceImplementation implements googleService 
{
	@Value("${spring.social.facebook.app-id}")
	String gooleid;
	@Value("${spring.social.facebook.app-secret}")
	private String secret;
	
	private FacebookConnectionFactory createfacebookconnection()
	{
		return new FacebookConnectionFactory(gooleid,secret);
	}

	@Override
	public String goolelogin() {
		OAuth2Parameters perameter=new OAuth2Parameters();
		perameter.setRedirectUri("http://localhost:9091/facebook");
		perameter.setScope("public_profile,email");
		return createfacebookconnection().getOAuthOperations().buildAuthenticateUrl(perameter);
		
	}

	@Override
	public String getfacebookAcessToken(String code) {
		return createfacebookconnection().getOAuthOperations()
		.exchangeForAccess(code, "http://localhost:9091/facebook", null).getAccessToken();
	}

	@Override
	public User getfacebookuserprofile(String acesstoken) {
		Facebook facebook=new FacebookTemplate(acesstoken);
		String[] fields= {"id","first_name","last_name","email"};
		return facebook.fetchObject("me", User.class,fields);
	}

}
