package com.example.Service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.example.Dao.IDatabasefilerepo;
import com.example.Entity.Databasefile;
import com.example.Exceptions.StorageException;


@Service
public class Databaseserviceimplementation implements IDatabaseService
{
	@Autowired
	IDatabasefilerepo datbaserepo;

	@Override
	public Databasefile storeFile(MultipartFile file, Databasefile user) {
		 // Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (fileName.contains("..")) {
               throw new StorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }
            if(file.isEmpty()) {
            	throw new StorageException("Failed to store empty file " + fileName);
            }

            Databasefile dbFile = new Databasefile();

            return datbaserepo.save(dbFile);
        } catch (Exception  ex) {
            throw new StorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
	}

	@Override
	public Databasefile getFile(String fileId) {
		return datbaserepo.findById(fileId).orElse(new Databasefile());
	}

	@Override
	public Databasefile save(Databasefile databasefile) {
		return datbaserepo.save(databasefile);
	}

	@Override
	public void deleteById(String theId) {
		datbaserepo.deleteById(theId);
		
	}

	@Override
	public List<Databasefile> findall() {
		return datbaserepo.findAll();
	}

	@Override
	public List<Databasefile> getgenderdress() {
		return datbaserepo.getgenderdress();
	}

	@Override
	public List<Databasefile> getWomendress() {
		return datbaserepo.getWomendress();
	}

	@Override
	public Databasefile searchBy(String theName) {
		Databasefile databasefile=datbaserepo.findByIdContainsOrFileNameContainsAllIgnoreCase(theName, theName);
		return databasefile;
			
	}

	@Override
	public List<Databasefile> getsamedress(String filename) {
		return datbaserepo.getsamedress(filename);
	}

	@Override
	public Databasefile getrealtedDress(String filename) {
		return datbaserepo.getrealtedDress(filename);
	}

	

	
}