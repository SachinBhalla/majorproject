package com.example.Service;

import com.example.Entity.User;


public interface IUserService 
{
	User SaveUser(User user);
	public User findByUserName(String userName);
	User findByEmail(String email);

}
