package com.example.Service;

import com.example.Entity.ConfirmationToken;

public interface IConfirmationTokenRepo {
	
	 ConfirmationToken findByConfirmationToken(String confirmationToken);
	 
	 ConfirmationToken save(ConfirmationToken confirmationtoken);
	 

}
