package com.example.Service;
import java.util.List;
import org.springframework.web.multipart.MultipartFile;
import com.example.Entity.Databasefile;



public interface IDatabaseService 
{
	public Databasefile storeFile(MultipartFile file,Databasefile user);
	
	public Databasefile getFile(String fileId);
	
	 public Databasefile save(Databasefile databasefile);
	 public void deleteById(String theId);
	 public List<Databasefile> findall();
     public List<Databasefile> getgenderdress();
     public List<Databasefile> getWomendress();
     public Databasefile searchBy(String theName);
     public List<Databasefile> getsamedress(String filename);
     
     public Databasefile getrealtedDress(String filename);
	 
	
	
}
