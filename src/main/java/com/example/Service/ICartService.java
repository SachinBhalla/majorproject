package com.example.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;


import com.example.Entity.CartItem;

public interface ICartService 
{
	
	public CartItem save(CartItem cartitem);
	public List<CartItem>getcartbyusername(String username);
	public void deleteById(String theId);
	public CartItem findByFileName(String filename);
	public CartItem findByusername(String username);
	public CartItem getdressforchecking(String filename,String username);
	
	

}
