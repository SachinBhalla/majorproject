package com.example.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;

import com.example.Dao.ConfirmationTokenRepository;
import com.example.Entity.ConfirmationToken;

public class confirmationTokenRepoImplementation implements IConfirmationTokenRepo
{
	@Autowired
	ConfirmationTokenRepository confirmationtokenrepo;

	@Override
	public ConfirmationToken findByConfirmationToken(String confirmationToken) {
		ConfirmationToken confirmationtoken=
				confirmationtokenrepo.findByConfirmationToken(confirmationToken);
		return confirmationtoken;
	}

	@Override
	public ConfirmationToken save(@RequestBody ConfirmationToken confirmationtoken) {
		return confirmationtokenrepo.save(confirmationtoken);
		
	}

}
