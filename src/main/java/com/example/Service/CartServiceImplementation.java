package com.example.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Dao.IAddToCartDao;
import com.example.Entity.CartItem;
@Service
public class CartServiceImplementation implements ICartService
{
	@Autowired
	IAddToCartDao cartdao;

	@Override
	public CartItem save(CartItem cartitem) {
		
		return cartdao.save(cartitem);
	}

	@Override
	public List<CartItem> getcartbyusername(String username) {
		return cartdao.getcartbyusername(username);
	}

	@Override
	public void deleteById(String theId) {
		cartdao.deleteById(theId);
	}

	@Override
	public CartItem findByFileName(String filename) {
		return cartdao.findByfilename(filename);
	}

	@Override
	public CartItem findByusername(String username) {
	return cartdao.findByusername(username);
	}

	@Override
	public CartItem getdressforchecking(String filename, String username) {
		
		return cartdao.getdressforchecking(filename, username);
	}

	
	

}
