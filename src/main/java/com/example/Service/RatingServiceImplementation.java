package com.example.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.Dao.IRating;
import com.example.Entity.Rating;
@Service
public class RatingServiceImplementation implements IRatingService
{
	@Autowired
	IRating ratingrepo;

	@Override
	public Rating SaveUserCommment(Rating rating) {
		return ratingrepo.save(rating);
	}

	@Override
	public List<Rating> getallcomments() {
		return ratingrepo.findAll();
	}
	

}
