package com.example.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="cart_data")
public class CartItem 
{
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	
	@Column(name="productname")
	private String productname;// Description
	
	@Column(name="price")
	private Integer price;
	
	@Column(name="file_name")
	private String filename;
	
	@Column(name="username")
	private String username;
	
   
	@Column(name="image")
	 @Lob
	private byte[] data;
	
	public CartItem(String productname, Integer price, String action,String username
			,String filename) {
		super();
		this.productname = productname;
		this.price = price;
		this.username=username;
		this.filename=filename;
		
	}
	public CartItem() 
	{
		super();
	}
	public byte[] getData() {
		return data;
	}
	public void setData(byte[] data) {
		this.data = data;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public String getFilename() 
	{
		return filename;
	}
	public void setFilename(String filename) 
	{
		this.filename = filename;
	}
	
	
	
}


