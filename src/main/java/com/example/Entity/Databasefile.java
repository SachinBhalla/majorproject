package com.example.Entity;
import org.hibernate.annotations.GenericGenerator;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "dress_data")
public class Databasefile {
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
    @Column(name="FileName")
    private String fileName;
    @Column(name="FileType")
	private String fileType;
    @Column(name="image")
	@Lob
	private byte[] data;
    @NotNull(message="is required")
    @Column(name="category")
	private String category;
    @NotNull(message="is required")
	@Column(name="price")
	private Integer price;
    @NotNull(message="is required")
	@Column(name="gender")
	private String gender;
    @NotNull(message="is required")
    @Column(name="Description")
    private String description;
    

	public Databasefile() {

	}

	public Databasefile(String fileName, String fileType, byte[] data,String gender,String description) 
	{
		this.fileName = fileName;
		this.fileType = fileType;
		this.data = data;
		this.gender=gender;
		this.description=description;
	}
	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public Databasefile(String fileName, String fileType, byte[] data, String category,Integer price,String description) 
	{
		super();
		this.fileName = fileName;
		this.fileType = fileType;
		this.data = data;
		
		this.category = category;
		this.price = price;
		this.description=description;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Integer getPrice() {
		return price;
	}

	public void setPrice(Integer price) {
		this.price = price;
	}

	
}