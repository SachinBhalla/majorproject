package com.example.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.example.Validation.ValidEmailServer;
import com.example.Validation.ValidPhoneServer;

@Entity
@Table(name="User_Record")
public class User 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	@NotNull(message="is Required")
	@Column(name="username")
	private String username;
	
	@NotNull(message="is Reqiured")
	@Column(name="password")
	private String password;
	
	@NotNull(message="is Reqiured")
	@Column(name="name")
	private String name;
	
	@NotNull(message="is Reqiured")
	@Column(name="number")
	@ValidPhoneServer
	private String number;
	
	@NotNull(message="is Reqiured")
	@Column(name="email")
	@ValidEmailServer
	private String email;
	
	@Column(name="roles")
	private String roles;

	public User(Integer id, @NotNull(message = "is Required") String username,
			@NotNull(message = "is Reqiured") String password, @NotNull(message = "is Reqiured") String name,
			@NotNull(message = "is Reqiured") String number, @NotNull(message = "is Reqiured") String email,String roles) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.name = name;
		this.number = number;
		this.email = email;
		this.roles=roles;
	}

	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}

	public User() {
		super();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	

}
