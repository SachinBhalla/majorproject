package com.example.Entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Entity
@Table(name="Ratings")
public class Rating 
{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	@Min(value=1,message = "Must Be Greater Then Or Equal To One")
	@Max(value=10,message = "Must Be Less Then Or Equal To Ten")
	@Column(name="rating")
	private Integer rating;
	@Column(name="username")
	private String username;
	@Column(name="description")
	private String description;
	@Column(name="productid")
	private String productid;
	
	
	public Rating() {
		super();
	}
	public Rating(Integer rating, String username, String description, String productid) {
		super();
		this.rating = rating;
		this.username = username;
		this.description = description;
		this.productid = productid;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getRating() {
		return rating;
	}
	public void setRating(Integer rating) {
		this.rating = rating;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getProductid() {
		return productid;
	}
	public void setProductid(String productid) {
		this.productid = productid;
	}
	
	
	

}
