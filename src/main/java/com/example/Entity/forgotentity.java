package com.example.Entity;

import javax.validation.constraints.NotBlank;

import com.example.Validation.ValidEmailServer;


public class forgotentity 
{
	 @NotBlank(message = "isrequired")
	 @ValidEmailServer
	 private String email;
	 @NotBlank(message = "isrequired")
     private String password;
	 @NotBlank(message = "isrequired")
     private String confirmpassword; 
	
	 public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmpassword() {
		return confirmpassword;
	}

	public void setConfirmpassword(String confirmpassword) {
		this.confirmpassword = confirmpassword;
	}

	public forgotentity() {
		super();
	}

	public forgotentity(@NotBlank(message = "isrequired") String email,
			@NotBlank(message = "isrequired") String password,
			@NotBlank(message = "isrequired") String confirmpassword) {
		super();
		this.email = email;
		this.password = password;
		this.confirmpassword = confirmpassword;
	}
	
	

}
