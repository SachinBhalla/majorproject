package com.example;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.example.Dao.IUserDao;
import com.example.Entity.User;




@Service
public class MyUserDetailService implements UserDetailsService
{
	
	@Autowired
	IUserDao userdao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		//Employee user=emprep.findByemail(email);
       User user=userdao.findByUsername(username);
       
      
		
		if(user == null)
			throw new UsernameNotFoundException("  Not Found"+username);
		MyUserDetail mud = new MyUserDetail(user);
		return mud;
	}
	

}