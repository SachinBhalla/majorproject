package com.example;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.Entity.User;


public class MyUserDetail implements UserDetails 
{
	private String username;
	private String password;
	private String name;
	private String number;
	private String email;
	
	private List<GrantedAuthority> authorities;
	
	public MyUserDetail(User user)
	{
		//String encodedPassword = bCryptPasswordEncoder.encode(user.getPassword());
		this.username=user.getUsername();
		this.password=user.getPassword();
		this.name=user.getName();
		this.number=user.getNumber();
		this.email=user.getEmail();
		this.authorities = Collections.singletonList(new SimpleGrantedAuthority(user.getRoles()));
	}
	public MyUserDetail()
	{
		
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}
	@Override
	public String getPassword() {
		return password;
	}
	@Override
	public String getUsername() {
		return username;
	}
	@Override
	public boolean isAccountNonExpired() {
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
	 return true;
	}
	@Override
	public boolean isEnabled() {
	return true;
	}

	
}